package com.entity.view;

import com.entity.WupinleibieEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 物品类别
 * 后端返回视图实体辅助类   
 * @author
 * @email 
 */
@TableName("wupinleibie")
public class WupinleibieView  extends WupinleibieEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public WupinleibieView(){
	}
 
 	public WupinleibieView(WupinleibieEntity wupinleibieEntity){
 	try {
			BeanUtils.copyProperties(this, wupinleibieEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
