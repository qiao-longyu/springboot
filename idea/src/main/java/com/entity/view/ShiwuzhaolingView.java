package com.entity.view;

import com.entity.ShiwuzhaolingEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 失物招领
 * 后端返回视图实体辅助类   
 * @author
 * @email 
 */
@TableName("shiwuzhaoling")
public class ShiwuzhaolingView  extends ShiwuzhaolingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public ShiwuzhaolingView(){
	}
 
 	public ShiwuzhaolingView(ShiwuzhaolingEntity shiwuzhaolingEntity){
 	try {
			BeanUtils.copyProperties(this, shiwuzhaolingEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
