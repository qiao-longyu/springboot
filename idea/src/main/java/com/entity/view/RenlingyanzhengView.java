package com.entity.view;

import com.entity.RenlingyanzhengEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 认领验证
 * 后端返回视图实体辅助类   
 * @author
 * @email 
 */
@TableName("renlingyanzheng")
public class RenlingyanzhengView  extends RenlingyanzhengEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public RenlingyanzhengView(){
	}
 
 	public RenlingyanzhengView(RenlingyanzhengEntity renlingyanzhengEntity){
 	try {
			BeanUtils.copyProperties(this, renlingyanzhengEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
