package com.entity.view;

import com.entity.FaburenEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 发布人
 * 后端返回视图实体辅助类   
 * @author
 * @email 
 */
@TableName("faburen")
public class FaburenView  extends FaburenEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public FaburenView(){
	}
 
 	public FaburenView(FaburenEntity faburenEntity){
 	try {
			BeanUtils.copyProperties(this, faburenEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
